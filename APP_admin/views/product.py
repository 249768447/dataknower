import json
"""
from django.forms import model_to_dict
from basic.basic_response import DataPackage, jsonp_wrapped_response, jsonp_res_data
from basic.basic_response_result import MSG, CODE
from APP_admin.serializers import *


# list
def product_banner_list(request):
    if request.method == 'GET':
        data_list = Product_banner.objects.filter(status=1).first()
        ser = BannerSerializer(data_list)
        data_package = DataPackage().set_fields(dict(ser.data))
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T'], data=data_package))


def product_list(request):
    if request.method == 'GET':
        data_list = Product.objects.filter(status=1).first()
        ser = ProductSerializer(data_list)
        data_package = DataPackage().set_fields(dict(ser.data))
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T'], data=data_package))


def function_list(request):
    if request.method == 'GET':
        data = Function.objects.filter(status=1).order_by('sort')
        ser = FunctionSerializer(data, many=True)
        data_package = DataPackage().set_elements(list(ser.data))
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T'], data=data_package))


def advantage_list(request):
    if request.method == 'GET':
        data_list = Advantage.objects.filter(status=1).order_by('sort')
        ser = AdvantageSerializer(data_list, many=True)
        data_package = DataPackage().set_elements(list(ser.data))
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T'], data=data_package))


def apply_list(request):
    if request.method == 'GET':
        data_list = Apply.objects.filter(status=1).order_by('sort')
        ser = ApplySerializer(data_list, many=True)
        data_package = DataPackage().set_elements(list(ser.data))
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T'], data=data_package))


def case_list(request):
    if request.method == 'GET':
        data_list = Case.objects.filter(status=1).order_by('sort')
        ser = CaseSerializer(data_list, many=True)
        data_package = DataPackage().set_elements(list(ser.data))
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T'], data=data_package))


def product_relation_list(request):
    if request.method == 'GET':
        data_list = Product_relation.objects.filter(status=1).order_by('sort')
        ser = ProductRelationSerializer(data_list, many=True)
        data_package = DataPackage().set_elements(list(ser.data))
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T'], data=data_package))


# insert
def product_banner_insert(request):
    if request.method == 'POST':
        data_post = json.loads(request.body)
        obj = Product_banner.objects.create(
            img=data_post['img'],
            url=data_post['url'],
            title=data_post['title'],
            info=data_post['info'],
        )
        data_package = DataPackage().set_fields({
            'obj': model_to_dict(obj)
        })
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T'], data=data_package))


def product_insert(request):
    if request.method == 'POST':
        data_post = json.loads(request.body)
        obj = Product.objects.create(
            img=data_post['img'],
            url=data_post['url'],
            title=data_post['title'],
            info=data_post['info'],
        )
        data_package = DataPackage().set_fields({
            'obj': model_to_dict(obj)
        })
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T'], data=data_package))


def function_insert(request):
    if request.method == 'POST':
        data_post = json.loads(request.body)
        obj = Function.objects.create(
            img=data_post['img'],
            url=data_post['url'],
            title=data_post['title'],
            info=data_post['info'],
            sort=data_post['sort'],
        )
        data_package = DataPackage().set_fields({
            'obj': model_to_dict(obj)
        })
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T'], data=data_package))


def advantage_insert(request):
    if request.method == 'POST':
        data_post = json.loads(request.body)
        obj = Advantage.objects.create(
            img=data_post['img'],
            url=data_post['url'],
            title=data_post['title'],
            info=data_post['info'],
            sort=data_post['sort'],
        )
        data_package = DataPackage().set_fields({
            'obj': model_to_dict(obj)
        })
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T'], data=data_package))


def apply_insert(request):
    if request.method == 'POST':
        data_post = json.loads(request.body)
        obj = Apply.objects.create(
            img=data_post['img'],
            url=data_post['url'],
            title=data_post['title'],
            info=data_post['info'],
            sort=data_post['sort'],
        )
        data_package = DataPackage().set_fields({
            'obj': model_to_dict(obj)
        })
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T'], data=data_package))


def case_insert(request):
    if request.method == 'POST':
        data_post = json.loads(request.body)
        obj = Case.objects.create(
            img=data_post['img'],
            url=data_post['url'],
            title=data_post['title'],
            info=data_post['info'],
            sort=data_post['sort'],
        )
        data_package = DataPackage().set_fields({
            'obj': model_to_dict(obj)
        })
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T'], data=data_package))


def product_relation_insert(request):
    if request.method == 'POST':
        data_post = json.loads(request.body)
        obj = Product_relation.objects.create(
            img=data_post['img'],
            url=data_post['url'],
            title=data_post['title'],
            info=data_post['info'],
            sort=data_post['sort'],
        )
        data_package = DataPackage().set_fields({
            'obj': model_to_dict(obj)
        })
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T'], data=data_package))


# update
def product_banner_update(request, id):
    if request.method == 'POST':
        data_post = json.loads(request.body)
        Product_banner.objects.filter(id=id).update(
            img=data_post['img'],
            url=data_post['url'],
            title=data_post['title'],
            info=data_post['info'],
        )
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T']))


def product_update(request, id):
    if request.method == 'POST':
        data_post = json.loads(request.body)
        Product.objects.filter(id=id).update(
            img=data_post['img'],
            url=data_post['url'],
            title=data_post['title'],
            info=data_post['info'],
        )
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T']))


def function_update(request, id):
    if request.method == 'POST':
        data_post = json.loads(request.body)
        Function.objects.filter(id=id).update(
            img=data_post['img'],
            url=data_post['url'],
            title=data_post['title'],
            info=data_post['info'],
            sort=data_post['sort'],
        )
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T']))


def advantage_update(request, id):
    if request.method == 'POST':
        data_post = json.loads(request.body)
        Advantage.objects.filter(id=id).update(
            img=data_post['img'],
            url=data_post['url'],
            title=data_post['title'],
            info=data_post['info'],
            sort=data_post['sort'],
        )
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T']))


def apply_update(request, id):
    if request.method == 'POST':
        data_post = json.loads(request.body)
        Apply.objects.filter(id=id).update(
            img=data_post['img'],
            url=data_post['url'],
            title=data_post['title'],
            info=data_post['info'],
            sort=data_post['sort'],
        )
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T']))


def case_update(request, id):
    if request.method == 'POST':
        data_post = json.loads(request.body)
        Case.objects.filter(id=id).update(
            img=data_post['img'],
            url=data_post['url'],
            title=data_post['title'],
            info=data_post['info'],
            sort=data_post['sort'],
        )
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T']))


def product_relation_update(request, id):
    if request.method == 'POST':
        data_post = json.loads(request.body)
        Product_relation.objects.filter(id=id).update(
            img=data_post['img'],
            url=data_post['url'],
            title=data_post['title'],
            info=data_post['info'],
            sort=data_post['sort'],
        )
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T']))


# 发布 | 撤回
def product_option(request, id):
    if request.method == 'POST':
        data_post = json.loads(request.body)
        Product.objects.filter(id=id).update(
            status=data_post['status']
        )
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T']))


def function_option(request, id):
    if request.method == 'POST':
        data_post = json.loads(request.body)
        Function.objects.filter(id=id).update(
            status=data_post['status']
        )
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T']))


def advantage_option(request, id):
    if request.method == 'POST':
        data_post = json.loads(request.body)
        Advantage.objects.filter(id=id).update(
            status=data_post['status']
        )
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T']))


def apply_option(request, id):
    if request.method == 'POST':
        data_post = json.loads(request.body)
        Apply.objects.filter(id=id).update(
            status=data_post['status']
        )
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T']))


def case_option(request, id):
    if request.method == 'POST':
        data_post = json.loads(request.body)
        Case.objects.filter(id=id).update(
            status=data_post['status']
        )
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T']))


def product_relation_option(request, id):
    if request.method == 'POST':
        data_post = json.loads(request.body)
        Product_relation.objects.filter(id=id).update(
            status=data_post['status']
        )
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T']))


# delete
def function_delete(request, id):
    if request.method == 'POST':
        Function.objects.filter(id=id).delete()
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T']))


def advantage_delete(request, id):
    if request.method == 'POST':
        Advantage.objects.filter(id=id).delete()
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T']))


def apply_delete(request, id):
    if request.method == 'POST':
        Apply.objects.filter(id=id).delete()
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T']))


def case_delete(request, id):
    if request.method == 'POST':
        Case.objects.filter(id=id).delete()
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T']))


def product_relation_delete(request, id):
    if request.method == 'POST':
        Product_relation.objects.filter(id=id).delete()
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T']))
"""

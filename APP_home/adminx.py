from django.contrib import admin
import xadmin
# Register your models here.
from . import models

xadmin.site.register(models.Swiper)
xadmin.site.register(models.Company)
xadmin.site.register(models.Service)
xadmin.site.register(models.Client)

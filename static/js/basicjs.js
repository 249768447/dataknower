$(document).ready(function () {
    "use strict";

    var $all = $(".pageBtn ul li");
    var $prev = $(".pageBtn ul li.btn.activeBtn").prevAll();
    var $next = $(".pageBtn ul li.btn.activeBtn").nextAll();
    var none = 'no';

    function adjustBtn() {
        if ($(window).width() <= 768) {
            for (var i = 0, j = $prev.length; i < j; i++) { // 隐藏当前页之前
                if (i < j - 2) {
                    $($prev[i]).addClass(none);
                }
            }

            for (var i = 0, j = $next.length; i < j; i++) { // 隐藏当前页之后
                if (j != 2) { // 代表最后一页
                    if (i < j - 2) {
                        $($next[i]).addClass(none);

                    }
                }
            }
        } else {
            $all.removeClass(none);
        }

    }

    adjustBtn();

    $(window).resize(function () {
        adjustBtn();
    });

    function adjustImg() {
        var w_height = $(window).height();
        var h_height = $("html").height();
        var con_height = $(".header").outerHeight() + $(".footer").outerHeight();
        console.log(w_height, h_height);
        if (w_height > h_height) {
            $(".search.null .con .imgcon").css('height', w_height - con_height);
        }
    }
    adjustImg();

    $(window).resize(function () {
        adjustImg();
    });


});

from django.apps import AppConfig


class AppAdminConfig(AppConfig):
    name = 'APP_admin'

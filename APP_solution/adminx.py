from django.contrib import admin
import xadmin
# Register your models here.
from . import models

xadmin.site.register(models.Solution_banner)
xadmin.site.register(models.Solution)
xadmin.site.register(models.Sol_summarize)
xadmin.site.register(models.Sol_advantage)
xadmin.site.register(models.Sol_apply)
xadmin.site.register(models.Sol_case)
xadmin.site.register(models.Solution_relation)

from django import template
register = template.Library()


@register.inclusion_tag('footer.html')
def footer():
    return {'dk_name': 'DataKnower'}

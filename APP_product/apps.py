from django.apps import AppConfig
import os

class AppProductConfig(AppConfig):
    name = 'APP_product'
    verbose_name = '产品管理'

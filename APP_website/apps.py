from django.apps import AppConfig


class AppWebsiteConfig(AppConfig):
    name = 'APP_website'

# Generated by Django 2.2.3 on 2019-12-31 22:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('APP_home', '0002_auto_20191231_2217'),
    ]

    operations = [
        migrations.AlterField(
            model_name='client',
            name='img',
            field=models.ImageField(upload_to='images'),
        ),
        migrations.AlterField(
            model_name='company',
            name='img',
            field=models.ImageField(upload_to='images'),
        ),
        migrations.AlterField(
            model_name='service',
            name='img',
            field=models.ImageField(upload_to='images'),
        ),
        migrations.AlterField(
            model_name='swiper',
            name='img',
            field=models.ImageField(upload_to='images'),
        ),
    ]

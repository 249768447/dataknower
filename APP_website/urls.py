from django.urls import path
from .views import *


urlpatterns = [
    path('home/', home_data, name='home_data'),
    path('product/', product_data, name='product_data'),
    path('solution/', solution_data, name='solution_data'),

]


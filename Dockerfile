FROM centos:7 AS esport_server_old
RUN echo "export LC_ALL=zh_CN.UTF-8"  >>  /etc/profile
ENV TZ "Asia/Shanghai"
ENV LANG zh_CN.UTF-8
RUN yum -y --setopt=tsflags=nodocs update && \
    yum -y --setopt=tsflags=nodocs install python3 && \
    yum -y --setopt=tsflags=nodocs install python3-devel && \
    yum -y --setopt=tsflags=nodocs install gcc && \
    yum -y --setopt=tsflags=nodocs install mysql && \
    yum -y --setopt=tsflags=nodocs install mysql-devel && \
    yum -y --setopt=tsflags=nodocs install kde-l10n-Chinese -y && \
    yum -y --setopt=tsflags=nodocs reinstall glibc-common -y && \
    localedef -c -f UTF-8 -i zh_CN zh_CN.utf && \
    yum clean all

ENV LANG zh_CN.UTF-8
COPY ./src/esportsystem/requirements.txt .
COPY ./pip.conf /root/.pip/
RUN pip3 install -r /requirements.txt
import json

""""
from django.forms import model_to_dict
from basic.basic_response import DataPackage, jsonp_wrapped_response, jsonp_res_data
from basic.basic_response_result import MSG, CODE
from APP_admin.serializers import *

# list
def solution_banner_list(request):
    if request.method == 'GET':
        data_list = Solution_banner.objects.filter(status=1).first()
        ser = SolBannerSerializer(data_list)
        data_package = DataPackage().set_fields(dict(ser.data))
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T'], data=data_package))


def solution_list(request):
    if request.method == 'GET':
        data_list = Solution.objects.filter(status=1).first()
        ser = SolutionSerializer(data_list)
        data_package = DataPackage().set_fields(dict(ser.data))
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T'], data=data_package))


def sol_summarize_list(request):
    if request.method == 'GET':
        data = Sol_summarize.objects.filter(status=1).first()
        ser = SolSummarizeSerializer(data)
        data_package = DataPackage().set_fields(dict(ser.data))
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T'], data=data_package))


def sol_advantage_list(request):
    if request.method == 'GET':
        data_list = Sol_advantage.objects.filter(status=1).order_by('sort')
        ser = SolAdvantageSerializer(data_list, many=True)
        data_package = DataPackage().set_elements(list(ser.data))
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T'], data=data_package))


def sol_apply_list(request):
    if request.method == 'GET':
        data_list = Sol_apply.objects.filter(status=1).order_by('sort')
        ser = SolApplySerializer(data_list, many=True)
        data_package = DataPackage().set_elements(list(ser.data))
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T'], data=data_package))


def sol_case_list(request):
    if request.method == 'GET':
        data_list = Sol_case.objects.filter(status=1).order_by('sort')
        ser = SolCaseSerializer(data_list, many=True)
        data_package = DataPackage().set_elements(list(ser.data))
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T'], data=data_package))


def solution_relation_list(request):
    if request.method == 'GET':
        data_list = Solution_relation.objects.filter(status=1).order_by('sort')
        ser = SolutionRelationSerializer(data_list, many=True)
        data_package = DataPackage().set_elements(list(ser.data))
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T'], data=data_package))


# insert
def solution_banner_insert(request):
    if request.method == 'POST':
        data_post = json.loads(request.body)
        obj = Solution_banner.objects.create(
            img=data_post['img'],
            url=data_post['url'],
            title=data_post['title'],
            info=data_post['info'],
        )
        data_package = DataPackage().set_fields({
            'obj': model_to_dict(obj)
        })
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T'], data=data_package))


def solution_insert(request):
    if request.method == 'POST':
        data_post = json.loads(request.body)
        obj = Solution.objects.create(
            img=data_post['img'],
            url=data_post['url'],
            title=data_post['title'],
            info=data_post['info'],
        )
        data_package = DataPackage().set_fields({
            'obj': model_to_dict(obj)
        })
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T'], data=data_package))


def sol_summarize_insert(request):
    if request.method == 'POST':
        data_post = json.loads(request.body)
        obj = Sol_summarize.objects.create(
            img=data_post['img'],
            url=data_post['url'],
            title=data_post['title'],
            info=data_post['info'],
            sort=data_post['sort'],
        )
        data_package = DataPackage().set_fields({
            'obj': model_to_dict(obj)
        })
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T'], data=data_package))


def sol_advantage_insert(request):
    if request.method == 'POST':
        data_post = json.loads(request.body)
        obj = Sol_advantage.objects.create(
            img=data_post['img'],
            url=data_post['url'],
            title=data_post['title'],
            info=data_post['info'],
            sort=data_post['sort'],
        )
        data_package = DataPackage().set_fields({
            'obj': model_to_dict(obj)
        })
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T'], data=data_package))


def sol_apply_insert(request):
    if request.method == 'POST':
        data_post = json.loads(request.body)
        obj = Sol_apply.objects.create(
            img=data_post['img'],
            url=data_post['url'],
            title=data_post['title'],
            info=data_post['info'],
            sort=data_post['sort'],
        )
        data_package = DataPackage().set_fields({
            'obj': model_to_dict(obj)
        })
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T'], data=data_package))


def sol_case_insert(request):
    if request.method == 'POST':
        data_post = json.loads(request.body)
        obj = Sol_case.objects.create(
            img=data_post['img'],
            url=data_post['url'],
            title=data_post['title'],
            info=data_post['info'],
            sort=data_post['sort'],
        )
        data_package = DataPackage().set_fields({
            'obj': model_to_dict(obj)
        })
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T'], data=data_package))


def solution_relation_insert(request):
    if request.method == 'POST':
        data_post = json.loads(request.body)
        obj = Solution_relation.objects.create(
            img=data_post['img'],
            url=data_post['url'],
            title=data_post['title'],
            info=data_post['info'],
            sort=data_post['sort'],
        )
        data_package = DataPackage().set_fields({
            'obj': model_to_dict(obj)
        })
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T'], data=data_package))


# update
def solution_banner_update(request, id):
    if request.method == 'POST':
        data_post = json.loads(request.body)
        Solution_banner.objects.filter(id=id).update(
            img=data_post['img'],
            url=data_post['url'],
            title=data_post['title'],
            info=data_post['info'],
        )
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T']))


def solution_update(request, id):
    if request.method == 'POST':
        data_post = json.loads(request.body)
        Solution.objects.filter(id=id).update(
            img=data_post['img'],
            url=data_post['url'],
            title=data_post['title'],
            info=data_post['info'],
        )
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T']))


def sol_summarize_update(request, id):
    if request.method == 'POST':
        data_post = json.loads(request.body)
        Sol_summarize.objects.filter(id=id).update(
            img=data_post['img'],
            url=data_post['url'],
            title=data_post['title'],
            info=data_post['info'],
            sort=data_post['sort'],
        )
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T']))


def sol_advantage_update(request, id):
    if request.method == 'POST':
        data_post = json.loads(request.body)
        Sol_advantage.objects.filter(id=id).update(
            img=data_post['img'],
            url=data_post['url'],
            title=data_post['title'],
            info=data_post['info'],
            sort=data_post['sort'],
        )
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T']))


def sol_apply_update(request, id):
    if request.method == 'POST':
        data_post = json.loads(request.body)
        Sol_apply.objects.filter(id=id).update(
            img=data_post['img'],
            url=data_post['url'],
            title=data_post['title'],
            info=data_post['info'],
            sort=data_post['sort'],
        )
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T']))


def sol_case_update(request, id):
    if request.method == 'POST':
        data_post = json.loads(request.body)
        Sol_case.objects.filter(id=id).update(
            img=data_post['img'],
            url=data_post['url'],
            title=data_post['title'],
            info=data_post['info'],
            sort=data_post['sort'],
        )
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T']))


def solution_relation_update(request, id):
    if request.method == 'POST':
        data_post = json.loads(request.body)
        Solution_relation.objects.filter(id=id).update(
            img=data_post['img'],
            url=data_post['url'],
            title=data_post['title'],
            info=data_post['info'],
            sort=data_post['sort'],
        )
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T']))


# 发布 | 撤回
def solution_option(request, id):
    if request.method == 'POST':
        data_post = json.loads(request.body)
        Solution.objects.filter(id=id).update(
            status=data_post['status']
        )
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T']))


def sol_summarize_option(request, id):
    if request.method == 'POST':
        data_post = json.loads(request.body)
        Sol_summarize.objects.filter(id=id).update(
            status=data_post['status']
        )
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T']))


def sol_advantage_option(request, id):
    if request.method == 'POST':
        data_post = json.loads(request.body)
        Sol_advantage.objects.filter(id=id).update(
            status=data_post['status']
        )
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T']))


def sol_apply_option(request, id):
    if request.method == 'POST':
        data_post = json.loads(request.body)
        Sol_apply.objects.filter(id=id).update(
            status=data_post['status']
        )
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T']))


def sol_case_option(request, id):
    if request.method == 'POST':
        data_post = json.loads(request.body)
        Sol_case.objects.filter(id=id).update(
            status=data_post['status']
        )
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T']))


def solution_relation_option(request, id):
    if request.method == 'POST':
        data_post = json.loads(request.body)
        Solution_relation.objects.filter(id=id).update(
            status=data_post['status']
        )
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T']))


# delete
def sol_summarize_delete(request, id):
    if request.method == 'POST':
        Sol_summarize.objects.filter(id=id).delete()
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T']))


def sol_advantage_delete(request, id):
    if request.method == 'POST':
        Sol_advantage.objects.filter(id=id).delete()
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T']))


def sol_apply_delete(request, id):
    if request.method == 'POST':
        Sol_apply.objects.filter(id=id).delete()
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T']))


def sol_case_delete(request, id):
    if request.method == 'POST':
        Sol_case.objects.filter(id=id).delete()
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T']))


def solution_relation_delete(request, id):
    if request.method == 'POST':
        Solution_relation.objects.filter(id=id).delete()
        return jsonp_wrapped_response(jsonp_res_data(msg=MSG['T'], rc=CODE['T']))
"""

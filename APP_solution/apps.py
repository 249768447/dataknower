from django.apps import AppConfig


class AppSolutionConfig(AppConfig):
    name = 'APP_solution'
    verbose_name = '方案管理'

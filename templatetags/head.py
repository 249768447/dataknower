from django import template
register = template.Library()


@register.inclusion_tag('head.html')
def head():
    return {'title': 'DataKnower'}

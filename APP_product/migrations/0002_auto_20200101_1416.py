# Generated by Django 2.2.3 on 2020-01-01 14:16

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('APP_product', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelTable(
            name='advantage',
            table=None,
        ),
        migrations.AlterModelTable(
            name='apply',
            table=None,
        ),
        migrations.AlterModelTable(
            name='case',
            table=None,
        ),
        migrations.AlterModelTable(
            name='function',
            table=None,
        ),
        migrations.AlterModelTable(
            name='product',
            table=None,
        ),
        migrations.AlterModelTable(
            name='product_banner',
            table=None,
        ),
        migrations.AlterModelTable(
            name='product_relation',
            table=None,
        ),
    ]

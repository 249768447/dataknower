from ..models import *
from rest_framework import serializers


class SwiperSerializer(serializers.ModelSerializer):
    create_time = serializers.SerializerMethodField()
    def get_create_time(self, obj):
        return obj.create_time.strftime("%Y-%m-%d %H:%I:%S")

    class Meta:
        model = Swiper
        # fields = ('id', 'img', 'url', 'sort', 'create_time', 'update_time', 'status')
        fields = '__all__'




